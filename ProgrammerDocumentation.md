# General architecture

In the game, different systems are interacting with each other. Part of it is also happening in real time. To give the overall code some united structure and make it scalable, we decided to follow architecture concepts from a different software development area - frontend web application frameworks. First the idea was to apply the concepts only to UI code but later it ended up almost everywhere.

What are these concepts? In frameworks such as React or Vue the rendered UI consists of a hierarchy of **components**. For example one component can be the whole browser page, another a list of items in that page and another an item in that list. An important thing is that a component only knows about the components that it contains (that are below it in the component hierarchy). For example an item list initializes its items but doesn't know anything about where it is being placed. That makes components reusable (can be put into different places) and also decreases dependencies throughout the code. If a component wants to pass information to its parent component, it triggers an event to which the parent component can be subscribed. In our code the UI is for the most part separated into "components" which are classes that inherit from `UUserWidget`.

Another concept from frontend frameworks is the **store**. The store contains data that can be accessed by the UI components but isn't dependent on them. The UI components call methods on the store to change its content when a user does some action. Similarly the store triggers events when its contents change to notify the UI back. In our game a user clicks on a button in a recipe UI component to make a recipe. The UI component then finds the inventory contents in the store and calls methods to remove recipe ingredients from them and add into them the recipe result. These inventory operations trigger an event in the store. A UI component showing the whole inventory is subscribed to it and it causes it to refresh and show the new contents of the inventory.

In a similar way, we can look at the farming simulator garden as a kind of 3D UI. A garden field is a component that initializes and contains garden tile components. A player might pour water onto one of the tiles. That component forwards this action to a garden tile class located in the store that contains the state of the tile. If the pouring action succeeds, it triggers an event that the tile was changed. The garden tile component is subscribed to it and starts showing a water spot decal on top of itself.

## BP vs C++

The preferred way to write all the classes in this project is to put all of the logic into C++. All C++ classes should then have inherited blueprint classes where their `EditDefaultsOnly` parameters are set. There are however a lot of exceptions and the project does contain blueprint code.


# Storing state

Even though there is only one map in the game, the code supports moving between multiple maps. If some class contains data that should be available in more than one level, it should be registered into `UFLPersistenceSubsystem`. The registered classes should have `UFLGI` or `GIBP_` prefix in their name. There is no way to register `Actors`. Objects are registered under classes they inherit from. The whole subsystem acts as a kind of dependency injection container from which the stored objects can be easily accessed everywhere in the code. It is the store described in general architecture. Objects can be added to `UFLPersistenceSubsystem` if their classes are among `PersistentObjectClasses` in `UFLGameInstance`. `UFLGameInstance` also contains some often accessed persistent objects as properties.

Saving and loading the game is done by calling appropriate methods in `UFLSaveGameSubsystem`. Parts of the saving code were initially taken from https://www.tomlooman.com/unreal-engine-cpp-save-system/. The subsystem saves every actor or actor component in a level that inherits from `IFLSavable`. Similarly every object in `UFLPersistenceSubsystem` is saved if it inherits from `IFLSavable`. Saved are only properties that have a `SaveGame` attribute. Unreal Engine can't save polymorphic variables or containers. To accomplish this, it is necessary to write save hook methods on saved objects by inheriting from `IFLHasSaveHooks`. In the hooks, helper methods (e.g. `UFLSerializeGameSubsystem::SerializePolymorphicSingle`) can be used for serializing entities, the serialized output can be put to byte arrays marked with `SaveGame` on the saved object.

# Timers

`UFLTimerHandles` is a wrapper class over native timers. It supports serialization and setting speed of multiple timers at once. Apart from visual and other effects, this class should always be used for timers. `UFLGITimeManager` is a store class that controls the day-night cycle and affects the speed of all `UFLTimerHandles` timers. When the player goes to sleep, all timers get increased speeds. This ensures that all garden interactions happen even while the sleeping takes only several seconds.

# UI Notes

The preferred way to write UI components in this project is to first make a C++ class that inherits from `UUserWidget`. All of the logic should be written in C++. The widget style is to be defined in the inherited blueprint class with everything connected with C++ using `BindWidget`. As shown in https://benui.ca/unreal/ui-bindwidget/.

When switching from being in the 3D world (the mouse moves the camera and can't be seen) to opening an interactable UI widget (mouse is seen and doesn't move the camera), it is necessary to do it by using functions from `AFLHUD`. `OpenCustomWidgetByInteraction` can be used to automatically switch input mode and also optionally wrap the widget in commonly used wrapper widgets such as tabs or a border with a close button. `AFLHUD` also supports spawning of "modal widgets" on top of existing UI that prevent any other user action until they are resolved.



# Player actions

The player can do actions in the world. An interface that indicates that an object can be acted upon is `IFLInteractable`. It has a `LookAt` that returns available actions and some information about them that affect for example what buttons trigger them. Another method `Interact` is called on `IFLInteractable` if the player actually selected one of the available actions, this method should execute the action. If the player is in the world looking at an actor that has a `UFLInteractableComponent` (inheriting from `IFLInteractable` ) attached to it, `LookAt` and later optionally `Interact` is automatically called on the component. For a more complicated interaction, the component can "forward" the interaction to other `IFLInteractable` objects and even use `UFLInteractionMerger` to automatically merge available actions from multiple different objects.

Interacting with the looked at `UFLInteractableComponent` is handled in `APlayerController`. Apart from this, the class also reacts to all user keyboard input and also sets up camera and character models for dialogs. It could be refactored into more classes.

# Dialogs

 ![Dialog nodes](./images/DialogNodes.png)

A dialog tree is created from blueprint pure functions. The flow of the dialog is from right to left because that is also the flow of blueprint pure functions. The dialog tree should be the result of `UFLDialogComponent::CreateDialogTree()` which is called just before starting the dialog session. The dialog session automatically stops when a node has null as its following node. Here are the available dialog nodes:

- **One Line Node** - A single line of text said by a character.
- **Fork Node** - The player selects from multiple lines one that should be said next.
- **Callback Node** - Node that triggers a custom event and immediately skips to next node. Can be used for saving player choices to memory or playing sound effects.
- **Switch Node** - Allows to define a function that chooses the next node at dialog runtime. Note that selecting next nodes depending on circumstances can also be implemented using blueprint branching and variables during dialog tree construction. Although only a switch node function can access data modified by callback nodes in the same dialog session.
- **Loop Begin/End Node** - If after entering a Loop Begin Node the dialog should end, it instead returns back to the Loop Begin Node. That way it is easy to implement for example asking different questions in a Fork Node (as seen on the image above). Loop End Node is used to "break" the loop cycle. It is possible to have an inner loop inside another loop. Then Loop End Node breaks only the innermost loop.
- **GoTo Branch Node and Branch Label Node** - Label nodes are registered when the dialog flow goes through them. On encountering a GoTo Branch Node, the dialog starts executing new nodes from the label node it references. This jump "forgets" any encountered label or loop begin nodes on the way from the label node to the goto node.

# Items

Items are instances of `UFLItem`. They are data assets so their properties (such as `Image` or `Name`) can be defined in the editor and saved to individual files. When items are stored somewhere (e.g. inventory) they are usually wrapped in `FFLItemWithCount` structure. When we are comparing if some items with counts are of the same item, we are using pointer equality pointing to the `UFLItem` data asset which is immutable. Actually all data assets in this code base are expected to be immutable.

All items should be registered in `AllItems` property of `UFLGIItemManager`. The manager also contains arrays of special items such as `IndestructibleItems` where appropriate items should be placed.

## Batteries

Some special item classes inherit from `UFLItem`. There are types of batteries which can have some discrete amount of charge (usually from 1 to 3 of usually electricity). In order to stack battery items with the same charge on top of each other, the actual `UFLItem` stored in `FFLItemWithCount` is in fact a battery item of some type with some charge (this is needed for pointer equality mentioned before). This `UFLItem` has a reference to a `UFLBatteryTypeItem` as do other `UFLItem`s with different charges. Batteries can only contain `UFLUnholdableItems`.

## Container items

A container item can be for example a bucket that can store either water or oil in discrete amounts. To allow multiple buckets in the game would require having a unique bucket data asset for all possible stored items and stored item amounts. In this project a container has only as many `UFLItem` instances as there are different items that it can store. Stored item amount is stored converted to a float in the `Charge` property of `FFLItemWithCount`. An exception to this are mugs which can store 1 coffee but not more. There can be many mugs in the game and they can be stacked on top of eachother. Mugs appeared later in the development process so their integration to other game systems is a bit hacky. Container items can only contain `UFLUnholdableItems`.

## Attributes

Attributes are instances of `UFLAttributeItem`. They represent an amount of some property that some object has. It is for example sunlight or watered tile (how much is garden tile watered). They are not something the player can have in the inventory. The `Charge` property of `FFLItemWithCount` indicates how much of the attribute there is in something, the `Count` property isn't used.

## Resource exchange

In this game, items are often exchanged between entities. Figuring out what items can be exchanged between 2 entities is a bit complicated with containers and batteries and various side effects that can happen during the exchange. This problem is handled by `UFLResourceExchangeSubsystem` that exchanges items between `FFLParsedResources`. `FFLParsedResources` contains five different types of items - simple, unholdable, container, battery and attribute items. For each item type there is information about how much of the item can there be at most and how much is there currently. To give an example: when pouring water onto a plant, we can make a `FFLParsedResources` that has one container item (bucket with water) and another `FFLParsedResources` with an unholdable water item with max capacity one and current capacity 0. `UFLResourceExchangeSubsystem::ExchangeRandomResource` is checking for all possible item exchanges, randomly selects one and modifies the participating `FFLParsedResources`.

Some resource entities have their internal resource representation already as `FFLParsedResources`. In other cases it is necessary to construct `FFLParsedResources` just before the exchange and then use the exchange result to modify the original differently represented resources.

## Resource exchange graph

Sometimes two entities are exchanging resources with each other whenever it is possible. This might be a garden tile providing water or soil to a plant that grows on top of it or automation plants that automatically transfer resources to other plants. In the game, relationships between resource exchanging entities form an oriented graph. If one node of the graph is connected to another, it automatically gives resources to it whenever it is possible.

`UFLResourceExchangeNode` represents a node in the graph. It tracks its neighboring nodes and has a reference to `IFLHasResourceExchangeNode` object on which it calls `GetProvider` and `GetConsumer` that return `FFLParsedResources` on which `UFLResourceExchangeSubsystem::ExchangeRandomResource` will be called when making an interaction. Upon making a new connection, the newly connected nodes try to exchange resources. If the exchange succeeds, it means that the "out" node has just gained some resource and it is possible that this new resource might be given to other nodes the out node is connected to, so it tries to exchange resources with them. Similarly the "in'' node might now have some free resource space that was previously filled so it tries to fill the space by requesting resources from nodes that connect to it. These resource exchanges can happen transitively on more and more nodes but the final result should be that no more exchanges are possible anywhere in the graph. All of this is happening instantly, there is no other gameplay code that runs during the graph “stabilization”.

It is possible to create endless loops in the graph. Because of that, it is necessary to adjust the code that uses the graph so that it can't happen. For example some automation plants are returning empty resources from `GetProvider` and `GetConsumer` for some number of seconds after they make a successful exchange. That way the endless loops during graph stabilization are avoided and the automation also feels more "realistic" because moving multiple resources between plants isn't instant and takes some time.


If resources of `IFLHasResourceExchangeNode` has changed and it was not the result of the resource exchange graph, it is necessary to notify the graph about it using `OnResourcesAdded`, `OnResourcesRemoved` or `OnAttributesChanged` on `UFLHasResourceExchangeNode` to propagate the item changes further into other nodes.

Sometimes a node consumes or provides different resources based on how it is connected to something. A resource might for example be exchanged only through veins. To that end, `GetProvider` and `GetConsumer` are called with types of connections as parameters when exchanging resources between two nodes. Two nodes (for example two plants) can be connected more than once and in more different ways.


When exchanging attribute items, the attributes only "propagate through the graph". The attribute charges aren't lowered when exchanged, they are only copied to other nodes that consume the same attributes and left unchanged in the original nodes. This way information that a tile has metal ore under it is only copied to the plant that grows on top of it. Some special attributes multiply the attribute value in its "out" nodes. Bonuses such as increasing fertility by 20% can be propagated by providing a fertility of 1.2 and setting attribute propagation to multiply.


## Moving items between slots

In the UI of the game, items are being moved between multiple areas (e.g. from the player inventory to a chest). The items can be dragged, shift clicked to automatically move to another area or right clicked to divide an item stack. To support this functionality, an area with items must inherit from `IFLItemArea`. The interface has virtual methods that enable the above described functionality. An `IFLItemArea` can be registered to `UFLGIItemManager` to enable the shift click item move. `IFLItemArea` has a `GetMovePriority` which returns a priority for the shift click move. When deciding where to move an item, an algorithm is checking areas (if it can move the item to the area) ordered from highest to lowest priority. After that, it checks the area where the move originated. For example shift clicking items in inventory can move the items between a bar and a grid that are part of the inventory, the inventory as a whole is only one area.

If an item area is internally represented as `TArray<FFLItemWithCount>`, the virtual functions for `IFLItemArea` are in most cases the same. Functions prefixed with `InventoryLike` on `UFLGIItemManager` are already implementing item moving functionality for the array and can be called inside `IFLItemArea` function overrides.

## Showing items in UI

Widget of class `UFLUWItem` should be used when displaying the `FFLItemWithCount` item on screen. It can be used in several modes - for dragging it to other areas, clicking on it in a selection or just displaying it without any actions available to the player. The right blueprint class for `UFLUWItem` can be found in `UFLGICommonAssets` along with other common classes used in multiple places in the code. 

`UFLUWItem` also displays the right charge of batteries or container items and can be styled using a `Style` data asset on `UFLItem`. It also has a tooltip to which custom widgets based on item type can be inserted. The custom widget classes are defined in `UFLUWItemTooltip::ItemsToCustomTooltipParts`.


# Stages

The content of the game is unlocked in stages. A stage is a data asset of class `UFLStage`. It can be assigned to items, recipes and other objects. Stages are used for example in the shop, where only items whose stages have been unlocked are available for purchase. Or only the crafting recipes whose stages have been unlocked are visible in the crafting tab. Generally it might be better to save as few state as possible and have other state dependent on it. Stages save the progression code from having to unlock many different things imperatively upon completing a quest for example. In development mode, it is also possible to manually switch stages and in that way quickly get to a certain point in the game.

# Quests

Quests are represented by classes inheriting from `UFLQuest`. Quests are further divided into tasks. Both quests and tasks have properties `bCompleted` and `bVisible`. The preferred way to write quests in this project is to have the quest class subscribe to events that complete tasks or otherwise change its state. The quest is subscribed to these events for the whole game. Because quests are persistent between levels and level actors which can affect quests aren't, it might be good to make event classes such as `BP_MainMapEvents` which persist between levels. When an actor in a level does something important, it broadcasts an event on the event class to which the quest is subscribed. In reaction, the quest might set one of its tasks to completed and another to visible. In this way, some quest tasks can be completed even before the quest is visible, although that is often the preferred behavior.


# Smaller game features

## Spawning wild flowers

At each point in the game, there exists a fixed amount of wild flowers at some positions on the main map taken from a predefined wild flower positions pool. When the player picks one flower up, another is immediately spawned on another random position from the pool.

## Inventory

Stores items in a grid and a bar. Items in the bar can be used while interacting with objects in the 3D world. When trying to add and remove inventory items at the same time (this might happen for example during crafting), use the `TryRemoveAndAddItems` method. The method first removes and adds the items on a copy of the inventory contents to ensure that it is possible and only then it modifies the inventory. When adding items to the inventory, there is always a problem that it might not have enough space. The outside code should somehow work even when the inventory is full.

## Chests

Actor components where items can be stored. Their implementation is similar to inventory.

## Shop

In the shop, items and recipes can be bought and certain items can be sold. Implementation of selling is done through triggering the same behavior as the shift click item move has but on only clicking on the sellable item in the inventory next to the shop. The item area of the shop "accepts" the item and increases the player's currency accordingly.

## Menu

Menu is the only larger collection of UI widgets implemented in Blueprint code. The same menu widget appears both in game and in the menu level, although some menu options are hidden in either state.

Certain graphics and sound options can be set in the `Settings` screen of the menu. This is done by setting properties of `UFLGameUserSettings` which inherits from `UGameUserSettings`. These values are automatically saved by Unreal Engine.

## Tips

Tips are rich text notes. A tip has a reference to a stage in which it should be unlocked.


## Herbiary

Herbiary displays all plants unlocked so far in the game with extended tooltips. It does it automatically for plantable plants (`UFLSeedItem`). Plants that are not plantable have to be defined in `UFLUWHerbiary::NonPlantablePlants`.

## Crafting

Recipes for crafting and also plant assembling are defined in `UFLGIRecipes`. It is also possible to add new recipes there throughout the game but the preferred way is to unlock recipes using stages. Recipes are data assets that can contain multiple variants of ingredients needed to craft the recipe result.



# Grafting aka Plant assembling

A plant assembling recipe consists of several plant part types:

- **Basic plant parts** - `UFLPartItem` items that are freely available to use during the creation of every grafting recipe (stems and leaves of a nice flower).
- **Fixed plant parts** - `UFLPartItem`s that should be placed on a fixed location (blooms of a nice flower). Can't be freely used as basic plant parts. Defined in `FFLFixedRecipePlantPart`.
- **Custom parts** - Their layout is defined uniquely for each recipe. Some limited amount of them can be used during the assembling. Right now they aren't used in any of the in game recipes. Defined in `FFLCustomRecipePlantPart`.

## Plant parts layout

 ![Grid layout](./images/GridLayout.png)

Each plant part has a defined layout which affects its shape and how it can connect to other plant parts. `UFLPartItem::GridPartImage` is an image with alpha channel that has the same aspect ratio as the part. It can be left empty to see a default visual of the part with connections marked by numbers. `UFLPartItem::Layout` is a structure of the part encoded in a string. The part consists of tiles. One tile is a 3x3 grid of letters. Examples:


- Empty tile:
    ```
    xxx
    xxx
    xxx
    ```

- Tile with no connections:
    ```
    xxx
    xLx
    xxx
    ```

- Tile with a connection of type 1 (plant connection) going up:
    ```
    x1x
    xLx
    xxx
    ```

- Tile with 2 connections of type 2 ("humanized" connections) going left and right:
    ```
    xxx
    2L2
    xxx
    ```

Connections can only have values from 1 to 9. They can only go out of the part. Tiles in the string need to form a rectangle (sometimes it is necessary to add empty tiles) and everything needs to be in correct format or the game might crash. The letters `x` and `L` were chosen because the editor font isn't monospace and those letters have the same width as numbers.

## Completing the grafting recipe

In order to assemble a plant, all fixed plant parts need to be placed in the right positions. Then all the parts in the grid need to be connected to each other by using either basic or custom parts. Connection types that are odd always need to be facing other connections of the same type from other plant parts (so a plant connection for example can't be connected to a wire connection or it can't just be facing an empty tile). Even connection types can face either the same connections or empty tiles.

An algorithm is checking whether the plant parts form a valid recipe every time the user moves a part on the plant assembling grid. It first finds a non-empty tile on a 2D array of tiles of placed plant parts. From this it runs a depth first search through part connections that should visit all placed plant parts. During this, all part connections facing each other are checked. Visited parts during the search are tracked in order to later check if all of the parts were visited (if all parts are connected). Independently, placement of fixed plant parts is also checked.

The dragging of plant parts is implemented in UI using relative positions of child part widgets to check where on grid the part currently is. Part widgets are draggable rectangles. Further improvement might be to make only the part of the plant part that has filled tiles draggable and ignore dragging initiated from empty tiles.


# Garden

Main class that controls the garden is `UFLGIGardenManager`. It handles planting and destroying new plants, stores all fields, plants and an instance of `UFLGarden`. Plants or more specifically objects representing what is currently on a garden tile are instances of `UFLPlant`. Position of a plant is given by an index of its `UFLField` and a position in that field (there are more garden fields in the game). The functionality of various types of plants is implemented in different `UFLPlantComponents`. They are not native Unreal Engine components because then plants would need to be actors and actors can't easily persist between levels. `UFLPlant` contains custom methods for adding, deleting or accessing components.

A plant and its behavior is defined using `UFLSeedItem` data asset. Components can be inserted into it directly in the editor. The components with their parameters are copied to a new instance of `UFLPlant` if the seed is planted. Unfortunately, it is necessary to add components to the seed item in a particular order, otherwise the game might crash. This order can now only be observed in already existing seed data assets.

## How are plants watered?

There are many classes involving plants. To return back to the pouring water example. A user tries to interact with a `UFLInteractablePlantComponent` that is attached either to a `AFLPlantView` that contains a collision box around a garden tile or is on a blueprint actor representing the plant's current 3D model that is also attached to `AFLPlantView`. The `UFLInteractablePlantComponent` forwards the interaction to `UFLPlant` that it has a reference to. The plant then cycles through all of its components and tries to interact with those that are interactable. `UFLPlant` also contains a `UFLTileComponent` apart from components specific to a currently planted plant. The player’s item (a bucket filled with water) exchanges resources with the tile component, giving it one water. This results in `UFLTileComponent` gaining a `WateredTile` attribute for a certain number of seconds. `UFLTileComponent` is connected via the resource exchange graph with a `UFLPlantResourcesComponent` of a plant planted on the same tile. The `WateredTile` attribute gets passed to the resources component. This removes one resource from the currently missing resources of the `UFLPlantResourcesComponent`. The plant resources component calls `AssignValuesToViewModel` on its `UFLPlant` which causes all plant components to have an option to modify `UFLPlantViewModel`. The plant resources component removes a water icon from the view model's notification items and calls a repaint function on `UFLPlant` which broadcasts a repaint event. `AFLPlantView` is subscribed to this event, it checks differences between its state and the new state in the `UFLPlantViewModel`. The change in notification items results in repainting of a widget showing the notification items above the `AFLPlantView` in the 3D world, indicating that the plant no longer needs water.

## Passing resources in the field

`UFLGarden` contains a sunlight attribute item, it sets its charge by reacting to events on `UFLGITimeManger`. This sunlight item gets past to all `UFLFields` via the resource exchange graph. In some fields the sunlight charge is modified if the field is in shade. Field attributes get again passed via the resource exchange graph to `UFLTileComponents` of all plants on the field. The field attributes are available to any other plant component that connects to it.

`UFLField` is also distributing some attribute items to `UFLTileComponents` on the start or load of the game. A field can have set counts of for example ores that it randomly distributes to chosen plant tiles. To keep the ores on the same places when the game loads again, the distribution algorithm requires a fixed seed value.


## Basic plant components

- **UFLEmptyTileComponent** - Handles planting of a new plant and displaying of a plant preview model. Automatically added to `UFLPlant` when it is empty.
- **UFLPlantedPlantComponent** - Automatically added to `UFLPlant` when it has a plant in the tile. Has a reference to the original seed item. The reference is used when displaying static information about the plant.
- **UFLTileComponent** - Always attached to `UFLPlant`. Accepts attributes from `UFLField`, has permanent attributes (usually set from `UFLField`), accepts items from the player which it converts to temporary attributes. Provides all of this to other plant components.
- **UFLPlantGrowthComponent** - This component is required on every `UFLSeedItem`. The cycle of a plant is divided into stages. There is a growth stage and multiple production stages. For example a stem plant has a longer growth stage after which it produces a stem and then keeps producing a stem at the end of each production stage (the production stages repeat endlessly). It is also possible to have no production stage - for example a potato plant is destroyed after the player first collects resources from it after the growth stage. It is also possible to not have a production stage as for example a miner does. Plant growth component initializes the timers for each stage, the duration for the stages is set in its parameters. It provides an API for other components to control its growth speed (change the speed or completely pause the growth). It notifies other components that a growth stage has ended by an event, this means that the plant is now in between growth stages, waiting for other components to call `StartNextGrowthStage` on it. Some components rely on the fact that this component is `bGrowing`, which means that the timer for the stage is moving. Based on the context, this can also mean that the plant is working (e.g. a miner might be working and mining ore). The component also handles setting of right plant 3D models (defined in `FFLPlantVisual`) to the plant view model based on how "grown up" the plant is. 
- **UFLPlantResourcesComponent** - This component is asking for resources defined in a `UFLPlantResources` data asset. The resources can be given to the plant by the player, it can take them from `UFLTileComponent`, or they can be given to it using automation via the resource exchange graph. It also forwards the resource exchange from automation to `UFLTileComponent`, allowing the forwarding only when the newly added item to the tile component leads to the tile component providing an attribute back to the plant resources component. For example it forwards water to the tile only if it just needs a watered tile attribute. The component controls `UFLPlantGrowthComponent` growth speed based on how well its resource needs are satisfied. If the growth component completes a stage, the plant resources component offers its harvest for the player to take or for automation to collect and move further. The harvest can be defined in a `OutcomeAfterHarvest` property or it can be taken from a component inheriting from `UFLProductGeneratorComponent` component. There are multiple classes that inherit from the product generator. They can generate random harvest, output different harvests in a sequence or output modified harvest based on what resources were provided to the plant resources component. After all of the harvest items are taken, the plant resources component starts another stage of the plant growth component. It can also store batteries inside that might for example fulfill electricity resource needs.
- **EndlessResourceComponent** - Provides an item that can be free to take by the player or automation when the UFLPlantGrowthComponent is `bGrowing`.

## Heart plant

A heart plant is an example of a plant that can store items inside of it. The items are stored in multiple chambers (`UFLStorageComponents` generated by `UFLMultipleStoragesComponent`), the chambers can only hold a single item type. The storage types can be set in a special tab in `UFLUWPlant`. When the type is set, items can be moved between the storage and the inventory. Different storage type widgets exist for simple item storages, unholdable item storages (the widget is a progress bar indicating how much of the unholdable item is in there) and battery item storages. Some automation components connect the `UFLStorageComponent`'s node to nodes of other plants in the resource exchange graph. The storage component exchanges resources when the `UFLPlantGrowthComponent` is `bGrowing`.


## Automation

Automation in the game works via the resource exchange graph. The heart plant allows access to stored items that take part in the automation. What will be described here is how the right connections in the resource exchange graph that allow automation are made. Automation components have a base class - `UFLAutomationComponent`. Generally automation components connect `UFLResourceExchangeNodes` of other components of plants. A way to keep the connections correct all the time is to have a function `RecreateConnections` that makes the connections around a certain automation plant from scratch using the current state of the garden. After creating the connections, all tile positions where placing or removing a plant might change what connection should exist is to be watched on plant change. If the plant change happens, existing connections are removed and new ones are created again using `RecreateConnections` using the new garden state. The watched tile positions are mostly in some fixed area as is the case of `UFLArmPlantLikeComponent` and `UFLEyePlantLikeComponent`. An exception is that some plants can be connected by veins, in which case all the tiles below the connecting veins should be temporarily added to the watched positions. 

What actual automation components are there?
- **UFLArmPlantLikeComponent** - Connects eight plants next to it with each other and also any heart plants that might be connected by veins to the arm plant. Also tracks which tiles next to it are empty so it can plant plants from seeds that it gets from heart plants.
- **UFLStorageAutomationComponent** - Represents a heart plant. Connects the heart plant to other humanized plants by veins (except for connecting to the arm plant, because the arm plant is already taking care of that connection). Connects to other heart plants only if the veins lead from it to the other heart plant (to prevent both plants from making the same connection). If two heart plants are directly next to each other, no connection is made. It would be unclear if resources should go to or from the original heart plant. If the resources went both ways, they would be randomly changing places between both heart plants, which is not something we wanted.
- **UFLEyePlantLikeComponent** - Used for passing defined attributes from the component to plants in some area. The component is part of the eye plant and nose plant.



# Visuals

A lot of geometry in the main level of the game is made using Unreal Engine landscape and foliage. The foliage has automatically generated LODs.

Some objects in the game are skeletal meshes that are animated using animation sequences that play in a loop. There is no animation blending or anything more complicated than the loops.

Particles that sometimes appear around plants are created using Niagara systems in Unreal Engine.


## Shaders

Some plants and foliage in the game are moved by the wind. In some cases this is done by using the `MF_PlantWind` material function. It uses `SimpleGrassWind` Unreal Engine node that adds world position offset to mesh vertices. The node's `WindWeight` parameter is increased with the mesh vertex local height, thus making the effect almost non-existent for roots of the plants while moving the plant's leaves and blooms that are higher.


`BP_PostProcessing` is a blueprint actor that sets parameters of `PP_CelShader` attached to a post process volume that spans the whole main level. The blueprint sets the parameters according to the current time in the day. The cel shader is highlighting edges close to the camera. It allows only 2 levels of light intensity (light and shadow). As objects are getting further away from the camera, they start fading into the shader's background color, creating a fog effect. Currently the level is very dark but is rendered much lighter thanks to heavy auto exposure.

Water splats and ores are rendered on top of the garden field mesh using Unreal Engine decals that are contained in blueprint actors such as `BP_WaterTile`. The field grid is rendered using `M_Field` material on the field mesh. The material takes field tile count and size and coordinates of the selected tile as parameters. It renders the grid by querying world position in a shader, computing if a pixel is in a grid and then rendering the grid using simplex noise and smooth transitions on the grid boundaries.

Rendering of plant preview models is done by setting a `M_PlantPreview` material to all material slots of a plant mesh. The preview models are slightly transparent.


# Sounds

All sounds in the game should be played using sound cues. The cues should either have a `Sound_SFX` or `Sound_Music` set as their sound class objects (it is needed to connect the cue to user sound volume settings). Sounds in the environment are played using audio components on actors. In reaction to a player action, a `PlaySound2D` blueprint node is usually executed with a set cue. If not overridden in `IFLInteractable::Interact`, a sound is played from `BP_PlayerController` after each successful interaction, chosen based on the player's used item and `EFLActionType`.

