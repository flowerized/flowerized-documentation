# Concept

Flowerized is a 3D story-driven, first-person farming simulator
with elements of adventure and puzzle game in which you play as Ilva,
an ex-astrobiologist from a very technologically developed planet system.
One day her ship accidentally goes through the teleport and crashes on an
abandoned planet.

She needs to get help from the inhabitants of this planet to repair the ship.
KT, the kettle robot tells her about research conducted by his creator Professor.
That includes growing plants in the garden, combining them with different ingredients
that can be found on this planet and many other things.

# Visuals

![Visuals](./images/Visuals.png)

Light tones predominate in combination with dark strokes and inclusions to
add volume to objects and darken them.

To enhance cartoonishness, a thin outline was used for objects.
Texture colours are mostly uniform with sharp colour transitions.

# Controls and Interaction

### Movement:

Walking - **WASD** <br />
Running - **Shift pressed** <br />
Camera - **Mouse** <br />
Jumping - **Space** <br />

### Interaction:

Several different keys are used for object interaction. Players can see options for the object in the
bottom right corner of the screen while pointing to the object. Options might differ depending
on the item chosen from the interaction tab.

![Interaction](./images/Interaction.png)

Switch an item on the interaction tab: **Mouse wheel or Number keys**

![ActionTab](./images/ActionTab.png)

### UI Elements:

Inventory: **Tab** <br />
Quest Log: **Q** <br />
Crafting: **C** <br />
Tips: **T** <br />
Herbiary: **H** <br />
Menu: **Esc** <br />

### Item Deletion:

To delete an item open the inventory by pressing **Tab**. Drag and drop an item to the Trash Bin then confirm the action.
Some important items cannot be deleted. You can also empty your bucket by dragging it to the 
Trash Bin (bucket itself cannot be deleted).

![TrashBin](./images/TrashBin.png)

# Interface

Player interface windows can be switched by using icons in the upper part of the screen. 
This icons are accessible when one of the windows is already opened.
The interface is controlled by **Mouse** and **LMB**.

![Inventory](./images/Inventory.png)

### Container Interface

Some of the game interface windows contain an inventory grid. Items on the inventory grid can be
dragged and dropped with a **Mouse** and **LMB** clicked. Most items stack up to 20 pieces and can
be split in half by clicking **RMB** on them. Container objects (e.g. Chest) interfaces contain several
grids and items can be moved between them by using **LMB** with **Shift** pressed or by the double
**LMB** click.

![Chest](./images/Chest.png)

### Menu

Ingame menu can be called from the game by pressing **Esc**. 
Several different functional and informational windows can be reached from there.

![Menu](./images/Menu.png)

# Garden

### Plants

There are 3 main types of plants:

- Simple plants
- Humanized plants
- Mechanical plants

Necessary resources vary between plant types. Plants within each type might serve various purposes.

### Plant Resources

To make a plant grow all the resource requirements should be met. 
Some resources are optional and serve to change the growing process or the outcome.
There are three main types of resources:

- Resources that are given to the plant (oil, strawberries, flies, etc.).
- Tile resources (water, iron, coffee, etc.).
- Resources received by other means (sunlight, nose plant effect, etc.).

Information about resources can be seen in the plant information interface (interaction button **E**)
or in the Herbiary. Some plants that can store items also have additional tabs in the information interface.

![PlantInfo](./images/PlantInfo.png)

### Growing

When the plant is fully grown it begins to perform its function or can be harvested. Some plants die
after being harvested and some can bear fruits again after giving it enough resources.
Many plants require different tools for harvesting. If a tool is required and which one can 
be seen on the icon above the fully-grown plant.

### Tiles

Tiles belong to a field and can contain different resources. Each tile on the field is visually
separated. Fields themselves might predetermine some
tiles characteristics, like the amount of sunlight that affects the growing time.

![Tile](./images/Tile.png)

# Game Flow

### Quest System

Questing is the determining game progression system. New things appear in the game while players
progress through a main quest line. Questing also serves as a tutorial system.
Besides the main quest line, the game contains optional quests that are not necessary to complete to finish the game.

![QuestLog](./images/QuestLog.png)

### Tips

Tips serve to introduce players to the game systems and how they work. New tips are added accordingly
to systems available for the current stage of the game.

![Tips](./images/Tips.png)

### Crafting

There are 2 different crafting systems in the game. Both require knowing a recipe and having enough
resources to create an item. Recipes are received as the game progresses and sometimes appear in the shop.

Simple crafting is called by pressing **C** or from other game interface windows and can
be accessed at any time.

![Crafting](./images/Crafting.png)

Plant assembling also requires solving a puzzle using ingredients the player has. To complete the puzzle,
a bloom (a sensor in case of mechanical plants) and a root should be connected and the resulting
plant on the grid should not have any cut off parts.
After completing a puzzle, the recipe can be used without doing puzzle again.
How many ingredients were used for a puzzle completion determines the recipe cost.

![Assembling](./images/Assembling.png)

### Herbiary

Herbiary contains information about all the plants currently available in the game.

![Herbiary](./images/Herbiary.png)

### Dialogues

Dialogues are one of the primary means to tell a story in the game. Dialogues tightly connected to quests
and quests usually end with a dialogue that might introduce new bits of the story or new plants.

![Dialogue](./images/Dialogue.png)

### Shop

Players can access the shop by interacting with a Merchant. The content of the shop updates throughout the game
and contains ingredients, recipes, plant seeds, etc. To earn money players can sell some items
to the Merchant. Money can be always earned by collecting and selling wild flowers that are spawning on the map.

![Shop](./images/Shop.png)

### Saving and Loading

The Saving window can be reached from the ingame menu. To create a new saving slot, type a name in the upper label
and click on the **Save** button. When choosing a slot from the list there is a possibility to rewrite it with new data or
delete the slot by clicking the **Delete** button.

![Saving](./images/Saving.png)

The saving window can be reached both from ingame menu and the main menu. To load or delete a saving slot label should be chosen
and the corresponding button should be clicked.

![Loading](./images/Loading.png)

The game is automatically saved into saving lots starting with **autosave_** after completing important quests.

### Day and Night Cycle

Sunlight can only be received during the day time. What part of the day is it now can be seen in the
upper right corner of the screen.

![DayNight](./images/DayNight.png)

During night time players can use a bed to skip the time until the next morning. The bed can be found inside the church.